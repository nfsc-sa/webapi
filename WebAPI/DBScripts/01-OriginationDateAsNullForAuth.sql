--USE [LoanMgmtSystem]
--GO
/****** Object:  StoredProcedure [dbo].[ValidateUserAccess]    Script Date: 3/31/2022 10:53:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:   <Harikrishna Bandi>  
-- Create date: <7/20/2021>  
-- Description: <to check is user credentails valid or not>  
-- exec ValidateUserAccess  1080431503,'' --'2019/02/20' --'20-02-2019'
-- =============================================  
ALTER PROCEDURE [dbo].[ValidateUserAccess] 
	@NationalID bigint,
	@OriginationDate varchar(50)=null
AS
BEGIN
	SET NOCOUNT ON;
	SELECT (b.FirstName+' '+b.LastName) AS FullName, c.OriginationDate,c.MortgageLoanAccountNumber,b.NationalID From [Beneficiary] b 
		INNER JOIN [dbo].[Contracts] c  ON b.[BeneficiaryID]=c.BeneficiaryID
		  Where b.NationalID=@NationalID
		  AND (ISNULL(LTRIM(RTRIM(@OriginationDate)), '') = ''  
		 OR CONVERT(DATE, c.OriginationDate, 103) = CONVERT(DATE, CONVERT(DATETIME, @OriginationDate, 103), 103))

END
