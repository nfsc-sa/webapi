USE [LoanMgmtSystem]
GO
/****** Object:  StoredProcedure [dbo].[GetWebBasicInformation]    Script Date: 12/7/2021 5:12:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:   <Harikrishna Bandi>  
-- Create date: <7/20/2021>  
-- Description: <to get basic details about contract for public web app>  
-- exec GetWebBasicInformation 71876614
-- =============================================  
ALTER PROCEDURE [dbo].[GetWebBasicInformation] 
 @AccountNumber bigint
AS
  BEGIN
	DECLARE @TotalPaidAmount DECIMAL(18, 2)
    DECLARE @LmsRefNo VARCHAR(20)

   SET @LmsRefNo= (SELECT cont.LMSReferenceNo FROM contracts cont   
						WHERE cont.MortgageLoanAccountNumber=@AccountNumber)  

  SET @TotalPaidAmount = (select sum(installmentno*installmentamount) from
									(select Count(InstallmentNo) as installmentno ,InstallmentAmount as installmentamount 
									from PaymentAllocation where LMSReferenceNo=@LmsRefNo group by InstallmentAmount)t)

SELECT cont.LMSReferenceNo, 
	cont.MortgageLoanAccountNumber, 
	b.NationalID,
	b.NIDExpiryDate,
	ct.ContractTypeInEn,
	cont.OriginationDate, 
	om.OriginatorNameInEn,
    (b.FirstName+' '+b.LastName) AS FullName,
	lom.NameInEnglish AS LegalOwner,
	b.MobileNumber,
	cont.MaturityDate,
	cont.CurrentArrearsBalance AS DelinquentBalance,
	cont.NumberOfPaidInstallments, 
	cont.OutstandingPrincipal,
	cont.DueAmount,
	cont.DueDate,
    cont.RemainingTerm,

	IT.InterestTypeOrigination,
	cont.InitialMonthlyInstallment,
	IT.OriginalAPR,
	cont.OriginalLoanAmount,
	cont.UnpaidPrincipalBalance,      
	cont.LastRepricingDate,
	cont.FinalPayment,
	cont.Outstanding,
	IT.InterestRateMargin,
	IT.TermCostRate,

	cont.CurrentLoanAmount,
	cont.TotalPayableAmount,
	cont.InitialTerm,
	cont.IsGuarantor,
	cont.IsRestructured,
	cont.InitalMaturityDate,
	cont.CurrentMonthlyInstallment,
    cont.InstallmentDay,
	cont.FirstPaymentDate,
	pfm.PaymentFrequencyInEn as PaymentFrequency,
    cont.DownPayment,
	@TotalPaidAmount as TotalPaidAmount

FROM Contracts cont      
		INNER JOIN Beneficiary b ON cont.BeneficiaryID = b.BeneficiaryID 
		INNER join InterestRate it ON cont.ContractID =IT.ContractID 
		LEFT join LegalOwnerMaster lom ON lom.LegalOwnerID=cont.LegalOwner
		INNER join ContractTypeMaster ct ON ct.ID =cont.ContractTypeID 
		LEFT join OriginatorMaster om ON om.ID=cont.OriginatorID
		LEFT Join [dbo].[PaymentFrequencyMaster] pfm ON pfm.PaymentFrequencyCode=cont.PaymentFrequencyCode
	WHERE b.IsDeleted = 0  
			AND cont.MortgageLoanAccountNumber=@AccountNumber
    
END
GO
/****** Object:  StoredProcedure [dbo].[GetWebPaymentScheduleDetail]    Script Date: 12/7/2021 5:12:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Harikrishna
-- Create date: 10/13/2021 
-- Description: <to get the payment schedules details for web app>
-- exec [GetWebPaymentScheduleDetail]  20190300000621
-- =============================================                 
ALTER PROCEDURE [dbo].[GetWebPaymentScheduleDetail]        
 @AccountNumber bigint  
AS                                 
BEGIN                       
    SELECT  Distinct --bpp.ID,
	--cont.MortgageLoanAccountNumber,
	bpp.InstallmentNo,
	CAST(bpp.InstallmentDate as date) as InstallmentDate,
	bpp.InstallmentAmount,
	bpp.InterestPaid as TermCost,
	bpp.CapitalPaid as PrincipalAmount,
	CASE        
    WHEN (bpp.InstallmentDate< GETDATE() or IsPaid = 1 )THEN (bpp.InstallmentAmount-(COALESCE(PA.PaidPrincipal,0) + COALESCE(PA.PaidTermCost,0)))        
    ELSE (null)        
	END AS RemainingAmount,
   IsPaid      
   FROM (select * from Contracts where MortgageLoanAccountNumber = @AccountNumber) cont                           
       --INNER JOIN property p ON cont.PropertyID = p.PropertyID                         
       INNER JOIN BeneficiaryPropertyPayment bpp ON cont.ContractID = bpp.ContractID              
		LEFT JOIN PaymentAllocation PA on PA.LMSReferenceNo = cont.LMSReferenceNo and PA.InstallmentDate =bpp.InstallmentDate
    order by bpp.InstallmentNo;              
END; 
GO
/****** Object:  StoredProcedure [dbo].[GetWebPaymentTransactionDetail]    Script Date: 12/7/2021 5:12:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author : Harikrishna Bandi  
-- Create Date: 14/10/2021        
-- Description: <to get the payment transaction details for web app>   
-- exec [GetWebPaymentTransactionDetail] 20190300000621  
--==========================================================================  
ALTER PROCEDURE [dbo].[GetWebPaymentTransactionDetail]   
 @AccountNumber bigint       
AS      
BEGIN      
  
 SELECT ROW_NUMBER() OVER (ORDER BY pt.TransactionDate ) AS TransactionID,
 --pt.TransactionID,
	 pt.TransactionAmount,
	 pt.TransactionCategory, 
	 CAST(pt.TransactionDate as date) as TransactionDate  
     FROM PaymentTransaction pt      
     INNER JOIN Contracts cont ON cont.ContractID = pt.ContractID and cont.MortgageLoanAccountNumber = CAST(@AccountNumber  as Numeric)      
     INNER JOIN Property p ON p.PropertyID = cont.PropertyID      
     where ISNULL(pt.IsSystemGenerated,0) <> 1 ORDER BY pt.TransactionDate;    
    
    END;
GO
