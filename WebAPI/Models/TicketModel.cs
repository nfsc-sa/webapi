﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPI.Models
{
    public class TicketModel
    {
        public List<TicketTypeMaster> TicketType { get; set; }
        public List<TicketCategoryMaster> TicketCategory { get; set; }
        public List<TicketSubCategoryMaster> TicketSubCategory { get; set; }
    }
}