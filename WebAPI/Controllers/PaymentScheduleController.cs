﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NLog;
using WebAPI.Models;

namespace WebAPI.Controllers
{

    public class PaymentScheduleController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();
        private GetPaymentScheduleLink_Result downloadlink = null;

        public HttpResponseMessage GetDownloadLink(string NationalID,string DocumentNumber)
        {
            try
            {
                logger.Info(" GetDownloadLink Controller method execution started");
                using (var context = new RTOEntities())
                {
                     downloadlink = context.GetPaymentScheduleLink(NationalID, DocumentNumber).FirstOrDefault();
                }
                if (downloadlink ==null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { status = "error", Message = "NationalID and Document Reference Number is not valid" }, Configuration.Formatters.JsonFormatter);
                }
                return Request.CreateResponse(HttpStatusCode.OK, new { downloadlink = downloadlink.DocumentLink.ToString(), DateTime = downloadlink.CreatedDate.ToString() }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "GetDownloadLink Controller method has error");
                return Request.CreateResponse(HttpStatusCode.OK, new { status = "error", Message = "Some error occurred" }, Configuration.Formatters.JsonFormatter);
            }
        }

    }
}
