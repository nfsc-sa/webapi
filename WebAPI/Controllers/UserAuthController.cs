﻿using WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Helpers;
using System.Configuration;
using NLog;

namespace WebAPI.Controllers
{
    public class UserAuthController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        //// <summary>
        /// Provides functionality to check the validation of accessing app is he/she valid customer are not.
        //// </summary> 
        // GET: api/UserAuth
        public HttpResponseMessage CheckValidUser(long NationalID, string ChannelType, DateTime? OriginationDate=null)
        {
            try
            {
                logger.Info("UserAuth Controller CheckValidUser method execution started");
                ValidateUserAccess_Result obj = new ValidateUserAccess_Result();
                var MobileNo = "";
                using (var context = new RTOEntities())
                {
                    obj = context.ValidateUserAccess(NationalID, OriginationDate?.ToString("dd-MM-yyyy")).FirstOrDefault();
                    MobileNo = context.Beneficiaries.Where(x => x.NationalID == NationalID).FirstOrDefault()?.MobileNumber.ToString();
                }
                if (obj != null)
                {
                    string numbers = "0123456789";
                    Random objrandom = new Random();
                    string strrandom = string.Empty;
                    int range = Convert.ToInt32(ConfigurationManager.AppSettings["OTPRange"]);
                    for (int i = 0; i < range; i++)
                    {
                        int temp = objrandom.Next(0, numbers.Length);
                        strrandom += temp;
                    }
                    using (var context = new RTOEntities())
                    {
                        var data = context.WebOTPMasters.Add(new WebOTPMaster()
                        {
                            CustomerID = NationalID,
                            MortgageLoanAccountNumber = obj.MortgageLoanAccountNumber,
                            OTP = strrandom,
                            DateTime = DateTime.Now,
                            Type = ChannelType,   //"Web",
                            Status = "Active",
                        });
                        context.SaveChanges();
                    }

                    var Message = ConfigurationManager.AppSettings["OTPMessage"];
                    int ExprieTime = Convert.ToInt32(ConfigurationManager.AppSettings["OTPExpire"]);
                    var MessageTemplate = string.Format(Message, strrandom, ExprieTime);
                    SMSOTPService.SendSms(ConfigurationManager.AppSettings["url"], ConfigurationManager.AppSettings["user"],
                                 ConfigurationManager.AppSettings["pass"], MobileNo.ToString(), MessageTemplate, ConfigurationManager.AppSettings["sender"]);

                    return Request.CreateResponse(HttpStatusCode.OK, new { status = "success", FullName = obj.FullName.ToString(), AccountNumber = obj.MortgageLoanAccountNumber.ToString() }, Configuration.Formatters.JsonFormatter);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { status = "error",message="Authentication Failed" }, Configuration.Formatters.JsonFormatter);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "UserAuth Controller CheckValidUser method has error");
                return Request.CreateResponse(HttpStatusCode.OK, new { status = "error", OTPError = "Some error occurred" }, Configuration.Formatters.JsonFormatter);

                //return Request.CreateResponse(HttpStatusCode.ExpectationFailed, new { UserID = NationalID.ToString() }, Configuration.Formatters.JsonFormatter);
            }
        }
    }
}
