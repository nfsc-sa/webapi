﻿using NLog;
using WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class TicketListController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        //// <summary>
        /// Provides functionality to get the list of Tickets Available.
        //// </summary>
        // GET: api/TicketList
        public IEnumerable<GetWebTicketList_Result> GetTicketlist(long AccountNumber)
        {
            IEnumerable<GetWebTicketList_Result> objTicketModel = null;
            try
            {
                logger.Info("TicketList Controller GetTicketlist method execution started");
                using (var context = new RTOEntities())
                {
                    objTicketModel = context.GetWebTicketList(AccountNumber).OrderByDescending(x => x.CreatedOn).ToList();
                }
                return objTicketModel;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "TicketList Controller GetTicketlist method has error");
                return null;
            }
        }
    }
}
