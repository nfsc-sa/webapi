﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class PaymentSchedulesController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        //// <summary>
        /// Provides functionality to get the payment schedules details of an account number.
        //// </summary>
        // GET: api/GetPaymentSchedules
        public IEnumerable<GetWebPaymentScheduleDetail_Result> GetPaymentSchedules(long AccountNumber)
        {
           List<GetWebPaymentScheduleDetail_Result> data = new List<GetWebPaymentScheduleDetail_Result>();
            try
            {
                logger.Info("PaymentDetails Controller GetPaymentSchedules method execution started");
                using (var context = new RTOEntities())
                {
                    data = context.GetWebPaymentScheduleDetail(AccountNumber).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "PaymentDetails Controller GetPaymentSchedules method has error");
            }
            return data;
        }
    }
}
