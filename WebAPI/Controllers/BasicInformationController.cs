﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;
using NLog;

namespace WebAPI.Controllers
{
    public class BasicInformationController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        //// <summary>
        /// Provides functionality to get the loan details of account number.
        //// </summary>
        // GET: api/BasicInformation
        public IEnumerable<GetWebBasicInformation_Result> GetBasicInfo(long AccountNumber)
        {
            GetWebBasicInformation_Result data = new GetWebBasicInformation_Result();
            try
            {
                logger.Info("BasicInformation Controller GetBasicInfo method execution started");
                using (var context = new RTOEntities())
                {
                    data = context.GetWebBasicInformation(AccountNumber).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "BasicInformation Controller GetBasicInfo method has error");
            }
            yield return data;
        }

    }
}
