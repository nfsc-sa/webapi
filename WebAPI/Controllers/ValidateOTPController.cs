﻿using NLog;
using WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class ValidateOTPController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        //// <summary>
        /// Provides functionality to insert the newly created Ticket Into db table.
        //// </summary>
        // GET: api/ValidateOTP
        public HttpResponseMessage ValidateAccessOTP(string otp, long AccountNumber, string ChannelType)
        {
            var result = string.Empty;
            var LMSCustomerID = string.Empty;
            try
            {
                logger.Info("ValidateOTP Controller ValidateAccessOTP method execution started");
                using (var context = new RTOEntities())
                {
                    var data = context.GetUserDetailsForApi(otp, AccountNumber).ToList();
                    if (data.Count == 0)
                    {

                        return Request.CreateResponse(HttpStatusCode.OK, new
                        {
                            status = "error",
                            OTPError = "wrong"
                        }, Configuration.Formatters.JsonFormatter);
                    }
                    else
                    {
                        // check for Expiry time 
                        int ExprieTime = Convert.ToInt32(ConfigurationManager.AppSettings["OTPExpire"]);
                        var valid = DateTime.Now.Subtract(data[0].DateTime).TotalMinutes <= ExprieTime;
                        if (valid == true)
                        {
                            context.WebOTPMasters.Where(x => x.OTP == otp).ToList()
                                            .ForEach(x => x.Status = "Validated");
                            context.SaveChanges();
                            string browserName = "";
                            HttpBrowserCapabilities browser = HttpContext.Current.Request.Browser;
                            browserName = browser.Browser;
                            context.WebLoginHistories.Add(
                                         new WebLoginHistory()
                                         {
                                             CustomerID = data[0].CustomerID.ToString(),
                                             Type = ChannelType,
                                             LoginDateTime = DateTime.Now,
                                             LogOutDateTime = null,
                                             BrowserName = browserName.ToString(),
                                         });
                            context.SaveChanges();
                            LMSCustomerID = data[0].CustomerID.ToString();
                            return Request.CreateResponse(HttpStatusCode.OK, new { status = "success", AccountNumber = AccountNumber.ToString() }, Configuration.Formatters.JsonFormatter);
                            //  return Request.CreateResponse(HttpStatusCode.OK, new { AccountNumber = AccountNumber.ToString() }, Configuration.Formatters.JsonFormatter);
                        }
                        else
                        {
                            context.WebOTPMasters.Where(x => x.OTP == otp).ToList()
                                            .ForEach(x => x.Status = "Expired");
                            context.SaveChanges();
                            return Request.CreateResponse(HttpStatusCode.OK, new { status = "error", OTPError = "expired" }, Configuration.Formatters.JsonFormatter);
                            // return Request.CreateResponse(HttpStatusCode.RequestTimeout, new { AccountNumber = AccountNumber.ToString() }, Configuration.Formatters.JsonFormatter);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "ValidateOTP Controller ValidateAccessOTP method has error");
                return Request.CreateResponse(HttpStatusCode.OK, new { status = "error", OTPError = "Some error occurred" }, Configuration.Formatters.JsonFormatter);
            }
        }

    }
}
