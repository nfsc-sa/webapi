﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class PaymentDetailsController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        //// <summary>
        /// Provides functionality to get the payment transactions details of an account number.
        //// </summary>
        // GET: api/GetPaymentDetails
        public IEnumerable<GetWebPaymentTransactionDetail_Result> GetPaymentDetails(long AccountNumber)
        {
            List<GetWebPaymentTransactionDetail_Result> data = new List<GetWebPaymentTransactionDetail_Result>();
            try
            {
                logger.Info("PaymentDetails Controller GetPaymentDetails method execution started");
                using (var context = new RTOEntities())
                {
                    data = context.GetWebPaymentTransactionDetail(AccountNumber).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "PaymentDetails Controller GetPaymentDetails method has error");
            }
            return data;
        }
    }
}
