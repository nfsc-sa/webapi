﻿using NLog;
using WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class TicketSelectionDetailsController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        //// <summary>
        /// Provides functionality to get the list of Tickets Available.
        //// </summary>
        // GET: api/TicketSelectionDetails
        public IEnumerable<TicketModel> GetTicketselections()
        {
            TicketModel objTicketModel = new TicketModel();
            try
            {
                logger.Info("TicketSelectionDetails Controller GetTicketselections method execution started");
                using (var context = new RTOEntities())
                {
                    objTicketModel.TicketType = GetTicketType().ToList();
                    objTicketModel.TicketCategory = GetTicketCategoryType().ToList();
                    objTicketModel.TicketSubCategory = GetTicketSubCategoryType().ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "TicketSelectionDetails Controller GetTicketselections method has error");
            }
            yield return objTicketModel;
        }


        #region Ticket type Dropdown selection lists  
        //// <summary>
        /// Provides functionality to get the list of Ticket Types.
        //// </summary>
        private IEnumerable<TicketTypeMaster> GetTicketType()
        {
            using (var context = new RTOEntities())
            {
                //TicketTypeMaster data = new TicketTypeMaster();
                var data = context.TicketTypeMasters.ToList();
                return data;
            }
        }
        //// <summary>
        /// Provides functionality to get the list of Ticket Category Types.
        //// </summary>
        private IEnumerable<TicketCategoryMaster> GetTicketCategoryType()
        {
            using (var context = new RTOEntities())
            {
                //TicketTypeMaster data = new TicketTypeMaster();
                var data = context.TicketCategoryMasters.ToList();
                return data;
            }
        }
        //// <summary>
        /// Provides functionality to get the list of Ticket Sub-Category Types.
        //// </summary>
        private IEnumerable<TicketSubCategoryMaster> GetTicketSubCategoryType()
        {
            using (var context = new RTOEntities())
            {
                //TicketTypeMaster data = new TicketTypeMaster();
                var data = context.TicketSubCategoryMasters.ToList();
                return data;
            }
        }
        #endregion
    }
}
