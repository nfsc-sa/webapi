﻿using NLog;
using WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class CreateNewTicketController : ApiController
    {
        Logger logger = LogManager.GetCurrentClassLogger();

        //// <summary>
        /// Provides functionality to insert the newly created Ticket Into db table.
        //// </summary>   //[FromBody]
        // POST: api/CreateNewTicket    
        public HttpResponseMessage CreateNewTicket(long AccountNumber, Guid TicketType, Guid TicketCategory, Guid? TicketSubCategory, string TicketDescription, string ChannelType)
        {
            TicketDetail objTicket = new TicketDetail();
            System.Guid TicketID;
            var TicketNumber = string.Empty;
            try
            {
                logger.Info(" CreateNewTicket Controller CreateNewTicket method execution started");
                using (var context = new RTOEntities())
                {
                    var contractID = context.Contracts.Where(x => x.MortgageLoanAccountNumber == AccountNumber).Select(x => x.ContractID).FirstOrDefault();
                    var DocumentType = "CRMTickets";
                    var TicketNumberSeq = context.USP_GetNextDocNo(DocumentType).ToList();
                    TicketNumber = TicketNumberSeq[0].TicketNumber;
                    TicketID = objTicket.TicketID = Guid.NewGuid();
                    objTicket.TicketReferenceNo = TicketNumber;
                    objTicket.ContractID = contractID;
                    objTicket.CreatedOn = DateTime.Now;
                    objTicket.IsActive = true;
                    objTicket.IsDeleted = false;
                    objTicket.TicketSubject = null;
                    objTicket.TicketType = TicketType;
                    objTicket.TicketCategoryID = TicketCategory;
                    objTicket.TicketSubCategoryID = TicketSubCategory;
                    objTicket.TicketDescription = TicketDescription;
                    objTicket.TicketStatusID = context.TicketStatusMasters.Where(x => x.ID == 4).FirstOrDefault().TicketStatusID;
                    objTicket.CreatedBy = context.Contracts.Where(x => x.ContractID == contractID).Select(x => x.BeneficiaryID).FirstOrDefault();
                    objTicket.DocumentPath = null;
                    objTicket.ModifiedBy = null;
                    objTicket.ModifiedBy = null;
                    objTicket.Channel = ChannelType;
                    context.TicketDetails.Add(objTicket);
                    context.SaveChanges();
                    context.TicketActionHistories.Add(
                              new TicketActionHistory()
                              {
                                  TicketActionID = System.Guid.NewGuid(),
                                  TicketID = TicketID,
                                  ActionId = context.ActionMasters.FirstOrDefault(x => x.ID == 4).ActionID,
                                  IsActive = true,
                                  Description = TicketNumber + " "
                                                   + context.ActionMasters.FirstOrDefault(x => x.ID == 4).ActionName,
                                  PreformedOn = DateTime.Now,
                                  PreformedBy = context.Contracts.Where(x => x.ContractID == contractID).Select(x => x.BeneficiaryID).FirstOrDefault()
                              });
                    context.SaveChanges();
                    var currentdate = DateTime.Now;
                    if (currentdate.Date == TicketNumberSeq[0].LastExecutedOn)
                    {
                        context.DocNumberMasters.Where(x => x.DocumentType == DocumentType).ToList()
                                .ForEach(x => x.MaxValue = TicketNumberSeq[0].MaxValue+1);
                    }
                    else
                    {
                        context.DocNumberMasters.Where(x => x.DocumentType == DocumentType).ToList()
                               .ForEach(x =>
                               {
                                   x.MaxValue = TicketNumberSeq[0].MaxValue+1;
                                   x.LastExecutedOn = DateTime.Now;
                               });
                    }
                    context.SaveChanges();
                }
                return Request.CreateResponse(HttpStatusCode.OK, new { NewTicketNumber = TicketNumber.ToString() }, Configuration.Formatters.JsonFormatter);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "UserAuth Controller CreateNewTicket method has error");
                return Request.CreateResponse(HttpStatusCode.OK, new { status = "error", OTPError = "Some error occurred" }, Configuration.Formatters.JsonFormatter);

                //return Request.CreateResponse(HttpStatusCode.ExpectationFailed, TicketNumber, Configuration.Formatters.JsonFormatter);
            }
        }

    }
}
