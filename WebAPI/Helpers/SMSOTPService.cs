﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace WebAPI.Helpers
{
    public class SMSOTPService
    {
        public string SendMessage(string url, string user, string pass, string to, string messageParameter, string sender)
        {
            return SendSms(url, user, pass, to, messageParameter, sender);
        }
        public static string SendSms(string url, string user, string pass, string to, string messageParameter, string sender)
        {
            int flag = 0, count = 0;
            string data = string.Empty;
            try
            {
                do
                {
                    var link = string.Format("{0}?user={1}&pass={2}&to={3}&message={4}&sender={5}", url, user, pass, to, messageParameter, sender);
                    count++;
                    using (var client = new HttpClient())
                    {
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        //client.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.Timeout = TimeSpan.FromMinutes(20);
                        var uri = new Uri(link);
                        var Send = client.GetAsync(uri).ContinueWith(task =>
                        {
                            if (task.Status == TaskStatus.RanToCompletion)
                            {
                                var responce = task.Result;
                                if (responce.IsSuccessStatusCode)
                                {
                                    flag = 1;
                                    data = responce.Content.ReadAsStringAsync().Result;
                                }
                            }

                        });
                        Send.Wait();
                    }
                } while (count <= 5 && flag == 0);
                //var status = InsertIntoSMSTrack(data, userID, messageTemplate, messageParameter, contractData);
            }
            catch (Exception ex)
            {
                var a = ex;
                //loggerx.Error(ex, "Send SMS method");
            }
            return data;
            //  return messageParameter;
        }
        //public static bool InsertIntoSMSTrack(string status, Guid? userID, int messageTemplate, string messageParameter)
        //{
        //    using (var context = new RTOEntities())
        //    {
        //        context.SMSTrackings.Add(
        //          new SMSTracking()
        //          {
        //              SMSID = System.Guid.NewGuid(),
        //              CreatedBy = userID,
        //              SMSStatus = status,
        //              CreatedOn = System.DateTime.Now,
        //              SMSType = messageTemplate,
        //              SMSText = messageParameter,
        //              SMSTo = contractData != null ? contractData.MobileNumber : null,
        //              IsSMSSend = status.ToLower().Contains("success") ? true : false,
        //              ContractID = contractData != null ? (Guid?)contractData.ContractID : null
        //          });
        //        context.SaveChanges();
        //    }
        //    return true;
        //}
    }
}